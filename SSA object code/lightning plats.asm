//0x80025c00 to 0x8005c000 is free to store for positions
//0xD900 worth of space for each plat
//0x1215 frames max

.orga 0x1206800

.dw 0x00090000
.dw 0x11010001
.dd 0x2A0000000404abf0 //pushable placeholder
.dw 0x2d000000
.dw 0x08000000
.dw 0x0C000000
.dw thisobjasm+0x7F200000
.dd 0x0c000000803839cc
.dw 0x09000000

thisobjasm:
//load object and mario's actions
ADDIU SP, SP, -0X18
sw ra, 0x14 (SP)
jal 0x802A3754 //is mario gp on this obj
nop
bne v0, r0, reset
nop
jal 0x802A3CFC //is mario on object
nop
lui t0, 0x8036
lw t0, 0x1160 (T0)
lw t1, 0x14c (T0)
//0 has no positions
//1 has positions already
//2 is storing
//3 is moving
//4 is waiting after movement
//5 is waiting to store
//6 is post reset

li t7, 6
beq t7, t1, waitreset
nop

beq t1, r0, waitmario

li t7, 5
beq t7, t1, store
lw t2, 0x188 (T0)


li at, 2
beq at, t1, storing

li t7, 3
beq t7, t1, moving

li at, 4
beq at, t1, wait
lw t1, 0x154 (T0)

//has positions
beq v0, r0, end
lw t1, 0x8033b17c
li t2, 0x0800034B
beq t1, t2, end
li at, 3
b end
sw at, 0x14c (T0)

moving:
lw t1, 0x154 (T0)
lw t6, 0x1a4 (T0) //ptr to store area
lw t4, 0x1b4 (T0) //length of movement
sll t4, t4, 1
slt t4, t1, t4
beql t4, r0, wait
sw r0, 0x154 (T0)
li at, -2
sw at, 0x210 (T0)
and t7, t1, at
sll t2, t7, 1
sll t3, t7, 2
addu t3, t2, t3 //time*6
addu t6, t3, t6 //ptr to area in struct
li.s f10, 2.0
andi t1, t1, 1
beq t1, r0, retardmode
li.s f10, 1.0
retardmode:
lui t8, 0x8034
lwc1 f2, 0x0 (T6)
lwc1 f4, 0xa0 (T0)
sub.s f14, f2, f4 //delta
div.s f12, f14, f10
add.s f2, f4, f12
lwc1 f6, 0xb1ac (T8)
add.s f6, f6, f12
swc1 f2, 0xa0 (T0)
lwc1 f2, 0x4 (T6)
lwc1 f4, 0xa4 (T0)
sub.s f16, f2, f4 //delta
div.s f12, f16, f10
add.s f2, f4, f12
swc1 f2, 0xa4 (T0)
lwc1 f2, 0x8 (T6)
lwc1 f4, 0xa8 (T0)
sub.s f14, f2, f4 //delta
div.s f12, f14, f10
add.s f2, f4, f12
beq v0, r0, end
swc1 f2, 0xa8 (T0)

lw t2, 0x210 (T0)
swc1 f6, 0xb1ac (T8)
lwc1 f8, 0xb1b4 (T8)
add.s f8, f8, f12
sw r0, 0x210 (T0)
beqz t2, end
swc1 f8, 0xb1b4 (T8)
li.s f10, 0.0
c.lt.s f16, f10
lwc1 f6, 0xb1b0 (T8)
bc1t end
add.s f6, f6, f16
b end
swc1 f6, 0xb1b0 (T8)




reset:
lw t0, 0x80361160
li at, 6
b home
sw at, 0x14c (T0)


waitreset:
bne v0, r0, end
nop
b end
sw r0, 0x14c (T0)




wait:
li t8, 4
sw t8, 0x14c (t0)
li at, 80
bne at, t1, end
li t2, 1
sw t2, 0x14c (T0)
home:
lwc1 f2, 0x164 (T0)
swc1 f2, 0xa0 (T0)
lwc1 f2, 0x168 (T0)
swc1 f2, 0xa4 (T0)
lwc1 f2, 0x16c (T0)
b end
swc1 f2, 0xa8 (T0)

waitmario:
beq v0, r0, end
li at, 5
b sparkles
sw at, 0x14c (T0)

store:
bne v0, r0, sparkles
li at, 0xd900
multu at, t2
mflo at
lui t2, 0x8002
ori t2, t2, 0x5c00
addu t2, t2, at //should be start pos
lui t8, 0x8034
lwc1 f2, 0xb1ac (T8)
lwc1 f4, 0xa0 (T0)
sub.s f4, f2, f4
swc1 f4, 0x170 (T0)

lwc1 f2, 0xb1b0 (T8)
lwc1 f4, 0xa4 (T0)
sub.s f4, f2, f4
swc1 f4, 0x174 (T0)

lwc1 f2, 0xb1b4 (T8)
lwc1 f4, 0xa8 (T0)
sub.s f4, f2, f4
swc1 f4, 0xe4 (T0)

li at, 2
sw at, 0x14c (T0)
b sparkles
sw t2, 0x1a4 (T0)

storing:
lw t6, 0x1a4 (T0) //ptr to store area
lui t8, 0x8034
lw t9, 0xb17c (T8)
andi t9, t9, 0x1c0
li at, 0x80 //in air
bne at, t9, stopstore
lw t1, 0x154 (T0)
sll t2, t1, 2
sll t3, t1, 3
addu t3, t2, t3 //time*12
addu t6, t3, t6 //ptr to area in struct
lwc1 f2, 0xb1ac (T8)
lwc1 f4, 0x170 (T0)
sub.s f2, f2, f4
swc1 f2, 0x0 (T6)
lwc1 f2, 0xb1b0 (T8)
lwc1 f4, 0x174 (T0)
sub.s f2, f2, f4
swc1 f2, 0x4 (T6)
lwc1 f2, 0xb1b4 (T8)
lwc1 f4, 0xe4 (T0)
sub.s f2, f2, f4
//b sparkles
swc1 f2, 0x8 (T6)


sparkles:
//13002af0
lw a0, 0x80361158
li a2, 0x13002af0
jal 0x8029edcc
li a1, 0
li.s f2, 80.0
lwc1 f4, 0xa4 (V0)
add.s f2, f2, f4
b end
swc1 f2, 0xa4 (V0)

stopstore:
li at, 1
sw at, 0x14c (T0)
sw t1, 0x1b4 (T0)

end:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18

//geo asm
DynamicSetTile:

ADDIU SP, SP, -0X18
sw ra, 0x14 (SP)
beqz a0, endgeo
nop

jal 0x80278f2c //alloc_display_list
li a0, 24 //2 cmds worth
//v0 is ptr to DL

lw t0, 0x8032df00 //gCurGraphNodeObject
beq t0, r0, endgeo
nop
lw t1, 0x14c (T0)
//0 has no positions - no scroll
//1 has positions already - normal scroll
//2 is storing - very slow scroll
//3 is moving - fast scroll
//4 is waiting after movement - scroll that slows down until at normal speed
//5 is waiting to store - very slow scroll
//6 is post reset - no scroll

//slow spd is 2 per frame
//normal speed is 8 per frame
//fast scroll is 24 per frame
//lose 1 spd every 10 frames
li t9, 0
beqz t1, scroll
li at, 6
beq at, t1, scroll
li t2, 2
beq t2, t1, scroll
li t9, 2
li at, 5
beq at, t1, scroll
li t2, 1
beq t2, t1, scroll
li t9, 4
li at, 3
beq at, t1, scroll
li t9, 12
//slowing down from here on out
lw t3, 0x154 (T0)
li at, 5
divu t3, at
mflo t3
subu t9, t9, t3

scroll:
//texture is 32x256, scroll in T axis
//*0404A8A0
lui at, 0x0600
sw at, 0x0 (V0)
li at, 0x0404a8a0
sw at, 0x4 (V0)

li at, 0xf2000000
lw t1, 0x1c8 (T0) //current T positions
addu t1, t1, t9
sw t1, 0x1c8 (T0)
andi t1, t1, 0xfff
addu at, at, t1
sw at, 0x8 (V0)

li at, 0x0007c3fc
sw at, 0xc (V0)

lui at, 0x0601
sw at, 0x10 (V0)
li at, 0x0404a930
sw at, 0x14 (V0)

endgeo:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18

.orga 0x20157c
.dw DynamicSetTile+0x7F200000