.org 0x120a500

.dw 0x00040000
.dw 0x11010001
.dw 0x2f000000
.dw 0x00000001
.dw 0x0e5c0040
.dw 0x27260000
.dw 0x08011a64 //flyguy anim
.dw 0x28000000
.dw 0x0e5d0040 //col sphere
.dw 0x2d000000 //set 0x168 to home Y
.dw 0x102b0000 //enable general interaction
.dw 0x08000000
.dw 0x10050000 //enable interaction w/ mario
.dw 0x0c000000
.dw gliderasm + 0x7F200000
.dw 0x09000000

gliderasm:
addiu sp, sp, 0xffe8
sw ra, 0x0014 (SP)


lui t0, 0x8036
lw t0, 0x1160 (T0)
lw t2, 0x14c (T0)
bne t2, r0, reset
lw t1, 0x134 (T0)
li at, 1
bne t1, at, drop
//if no branch, then rise up
lwc1 f2, 0xa4 (T0)
lh t9, 0x8033b194
//sh t9, 0xca (T0)
sh t9, 0xd6 (T0)
//sh t9, 0x8033b196
li.s f4, 8.0
lw t6, 0x1a4 (T0)
srl t7, t6, 1
addiu t6, t6, 1
subiu t4, t6, 0x20
sw t6, 0x1a4 (T0)
bgtz t4, nocap
li at, 0x20
sw at, 0x1a4 (T0)
nocap:
mtc1 t7, f8
cvt.s.w f8, f8 //anywhere from 0 to 16
add.s f8, f4, f8
add.s f8, f8, f2 //intended y pos
lw t2, 0x188 (T0) //max y pos
mtc1 t2, f10
c.lt.s f10, f8
nop
bc1t capypos
nop
b end
swc1 f8, 0xa4 (T0)

capypos:
b end
swc1 f10, 0xa4 (T0)


reset:
lui t3, 0x8034
li t7, 0x80 //in air
lwc1 f0, 0xb1b0 (T3)
lw t4, 0xb17c (T3)
andi t6, t4, 0x1c0
beq t7, t6, end
swc1 f0, 0xB22C (T3) //remove fall dmg
b end
sw r0, 0x14c (T0)


drop:
sw r0, 0x134 (T0)
lw t7, 0x1a4 (T0)
beq t7, r0, nofling
sw r0, 0x1a4 (T0)
li at, 1
sw at, 0x14c (T0)
lh t9, 0xd6 (T0)
lui t3, 0x8034
sh t9, 0xb19E (T3)
li t2, 0x01000882 //special triple jump
sw t2, 0xb17c (T3)
li.s f12, 45.0
li.s f14, 15.0
swc1 f14, 0xb1bC (T3)
swc1 f12, 0xb1c4 (T3)



nofling:
lwc1 f0, 0x168 (T0)
c.le.s f2, f0
lui at, 0x4120
bc1t setyhome
mtc1 at, f6
sub.s f0, f2, f6


setyhome:
swc1 f0, 0xa4 (T0)

end:
lw ra, 0x0014 (SP)
jr ra
addiu sp, sp, 0x0018