.orga 0x1206700

.dw 0x00090000
.dw 0x11010001
.dd 0x2A000000040480c0 
.dw 0x08000000
.dw 0x0C000000
.dw thisobjasm+0x7F200000
.dd 0x0c000000803839cc
.dw 0x09000000

thisobjasm:
//load object and mario's actions
ADDIU SP, SP, -0X18
SW RA, 0X14 (SP)

jal 0x802A3CFC //is mario on object
nop
lui t0, 0x8036
bne v0, r0, stop
lw t0, 0x1160 (T0)
//rotate

lw t1, 0x188 (T0)
addiu t1, t1, 32
slti t2, t1, 0x300
beqzl t2, captop
li t1, 0x300
captop:
b end
sw t1, 0x188 (T0)



stop:
lw t1, 0x188 (T0)
subiu t1, t1, 48
bltzl t1, capbot
li t1, 0x0
capbot:
sw t1, 0x188 (T0)

end:
lw t2, 0x188 (T0)
sh t2, 0x11a (T0)
lw t3, 0xd4 (T0)
addu t3, t3, t2
sw t3, 0xd4 (T0)


LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18