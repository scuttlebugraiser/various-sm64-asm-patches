//a press box
.org 0x1202684

hex{00 09 00 00
11 01 00 01
2A 00 00 00 04 03 89 B0
08 00 00 00
0c 00 00 00 80 40 26 a4
09 00 00 00}//

.org 0x12026a4

addiu sp, sp, $ffe8
sw ra, $0014 (SP)

lui t0, $8036
lw t0, $1160 (T0)
lw t5, $0154 (T0)
bne t5, r0, !init
lw t1, $0188 (T0)
sw t1, $00f0 (T0)

!init:
lui t9, $8034
lw t7, $b17c (T9)
andi t7, t7, $1c0
ori t6, r0, $140 //not directly controlling mario
beq t7, t6, !skipflipcheck
lbu t8, $b198 (T9) //frames since a was pressed
beq t8, r0, !flip
lui t0, $8036
lw t0, $1160 (T0)
lw t1, $00f0 (T0)
beq t1, r0, !endflip
nop
!skipflipcheck:
lui t0, $8036
lw t0, $1160 (T0)
lw t1, $00f0 (T0)
beq t1, r0, !endflip
nop
jal $803839cc
nop
beq r0, r0, !endflip
nop

!flip:
lui t0, $8036
lw t0, $1160 (T0)
lw t1, $00f0 (T0)
xori t1, t1, $0001
sw t1, $00f0 (T0)

!endflip:
lw ra, $0014 (SP)
jr ra
addiu sp, sp, $0018