.org 0x12038ac //switch

hex{00 09 00 00}
hex{11 01 00 01}
hex{2A 00 00 00 04 03 C5 B0}
hex{2D 00 00 00
08 00 00 00}
hex{0c 00 00 00 80 40 38 d8}
hex{0C 00 00 00 80 38 39 CC}
hex{09 00 00 00}


.org 0x12038d8

addiu sp, sp, $ffe8
sw ra, $14 (SP)

lui t0, $8036
lw t0, $1160 (T0)
li t1, $1
sw t1, $f0 (T0)
lw t9, $014c (T0)
bne t9, r0, !timer
sw t0, 0x18 (SP)
jal $802A3754 //is mario gp on obj
nop
beq v0, r0, !endspawner
lw t0, 0x18 (SP)
lw t1, $6c (T0) //child object
beq t1, r0, !endspawner
nop
lwc1 f12, $a0 (T1) //child x
lwc1 f14, $a4 (T1) //child y
jal $802F2B88 //spawn a star
lw a2, $a8 (T1) //child z
lw t0, 0x18 (SP)
lw t9, $0188 (T0) //bparam
lui at, $ff00
and t9, at, t9
sw t9, $0188 (V0) //sets bparam of star
lui t1, $3E00
sw t1, $014c (T0)
sw t1, 0x30 (T0) //y scaling
beq r0, r0, !endspawner
sw v0, $01e0 (T0) //ptr to star

!timer:
lw t5, $0154 (T0)
lhu t6, $018a (T0) //bparam 3/4
beq t5, t6, !killstar
lw v0, $01e0 (T0)
beq r0, r0, !endspawner
nop
!killstar:
sw r0, $14c (T0)
sh r0, $74 (v0)
lui a0, 0x3f80
sw a0, 0x30 (T0)

!endspawner:
lw ra, $14 (SP)
jr ra
addiu sp, sp, $0018


.org 0x1203988 //star spawner

hex{00 09 00 00}
hex{11 01 00 01
0c 00 00 00 80 40 39 b0
2a 00 00 00 04 03 A1 A0}
hex{08 00 00 00}
hex{0c 00 00 00 80 40 39 E4}
hex{09 00 00 00}

.org 0x12039b0
addiu sp, sp, $ffe8
sw ra, $14 (SP)

lui a0, $0040
jal $8029F95C //find obj
addiu a0, a0, $38AC
beq r0, v0, !noparent
lui t0, $8036
lw t0, $1160 (T0)
sw t0, $6c (V0)
sw v0, $6c (T0)
!noparent:
lw ra, $14 (SP)
jr ra
addiu sp, sp, $0018


.org 0x12039E4

addiu sp, sp, $ffe8
sw ra, $14 (SP)
lui t0, $8036
lw t0, $1160 (T0)
lw t1, $6c (T0) //parent obj
beq t1, r0, !solid
nop
lw t9, $1b0 (T1)
bne t9, r0, !endstar
nop
!solid:
jal $803839cc
nop
!endstar:
lw ra, $14 (SP)
jr ra
addiu sp, sp, $0018