
.INCLUDE "PRINT STRING NOTES.TXT"

//behavior for talking dorrie
.orga 0x21ed90
.dw 0x00040000
.dw 0x11012041
.dd 0x272600000600f638
.dw 0x28010000
.dw 0x08000000
.dd 0x0c0000008030b2f4 //darrie og asm
.dw 0x09000000

.orga 0xC62F4

addiu sp, sp, -0x18
sw ra, 0x14 (SP)

lw t0, 0x80361160
lB at, 0x188 (T0)
sw at, 0xf0 (T0)
lw t1, 0x14c (T0)
bne t1, r0, @@incutscene
lw t2, 0x154 (T0)
subiu t2, t2, 0x30
blez t2, @@endbob
lwc1 f0, 0x15c (T0)
lui t9, 0x44C0
mtc1 t9, f8
c.lt.s f0, f8
lw a1, 0x160 (T0)
bc1f @@endbob
lw a0, 0xd4 (T0)
jal 0x8029E530 //rotate towards obj
li a2, 0x0800
lw t0, 0x80361160
lui t8, 0x8034
lwc1 f2, 0xa4 (T0) //dorrie y
lwc1 f4, 0xb1b0 (T8) //mario y
c.lt.s f4, f2
lh t9, 0xb194 (T8)
bc1t @@endbob
subu t3, v0, t9 
sw v0, 0xc8 (T0)
sw v0, 0xd4 (T0)
addiu t3, t3, 0xA000
andi t3, t3, 0xffff
sltiu t3, t3, 0x4001
blez t3, @@endbob
lh t7, 0xafa2 (T8)
andi t7, t7, 0xc000
beq t7, r0, @@endbob
lw t6, 0xb17c (T8)
andi t6, t6, 0x1c0
beq t6, r0, @@startdiag
li t5, 0x180
bne t5, t6, @@endbob
@@startdiag:
addiu a0, t8, 0xb170
li a2, 0x20001306 //reading sign
jal 0x80252cf4
li a1, 0

lw t0, 0x80361160
lw a2, 0x188 (T0)
li at, 0x00FFFFFF
and a2, a2, at
lui at, 0x0500
jal 0x80277f50
addu a0, a2, at
or a2, r0, v0
li a0, 0x1a
jal STARTDIALOGWITHTHISOBJFUNC
li a1, 0xc0
lw t0, 0x80361160
li t9, 1
sw t9, 0x14c (T0)
li a0, 0x8033b170
jal 0x802509b8 //set mario anim
li a1, 0xc2 //anim ID
beq r0, r0, @@endbob


@@incutscene:
lw t8, 0x807f6df8
bne t8, r0, @@endbob
lui t7, 0x8034
li t6, 0x0C400201 //idle
sw t6, 0xb17c (T7)
sw r0, 0x14c (T0)
lui at, 0x8034
sw r0, 0xd480 (AT)
@@endbob:
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18