/**
kaze's code
deltaTime += newTime - oldTime;
    oldTime = newTime;
    while (deltaTime>1562744){
        deltaTime-= 1562744;
        if (sTimerRunning && gHudDisplay.timer < 17999) {
            gHudDisplay.timer += 1;
        }
        area_update_objects();
    }
	
0x17D878 = 1562744
0x464F = 17999
8024b3e4                play_mode_normal
8027b164                area_update_objects
8033b26C                gHudDisplay.timer
80325070                osGetTime
can just store old time at end of ram I guess
**/

//complete code: https://pastebin.com/Wdci4rQV

.orga 0x8024b4b0-0x80245000 //entry point
j hook+0x7F200000
nop

.orga 0x1200200
hook:
lb at, 0x802078c0+0x12
beqz at, nofskip
nop
jal 0x80325070 //osGetTime
sw s3, 0x28 (SP)
li s3, 0
DSLL32 v0, v0, 0
daddu v0, v1, v0
ld t0, 0x807ffff8 //old time
bnez t0, noinitold
nop
daddu t0, r0, v0
noinitold:
ld t1, 0x807ffff0 //delta time
daddu t2, t1, v0 //delta time+new time
dsubu t2, t2, t0 //delta time-old time
sd v0, 0x807ffff8 //old time = new time
while:
slti at, s3, 3
beqz at, fuck
li at, 1562744
dsubu t3, t2, at
blez t3, noupdate
lb t6, 0x8033b25e //sTimerRunning
beqz t6, notimer
lhu t7, 0x8033b26c //gHudDisplay.timer
slti at, t7, 17999
beqz at, notimer
addiu t7, t7, 1
sh t7, 0x8033b26c
notimer:
sd t3, 0x807ffff0 //delta time
jal 0x8027b164 //area_update_objects
addiu s3, s3, 1
ld t2, 0x807ffff0 //delta time
b while
nop

noupdate:
j 0x8024b4f0 //end point, update_hud_values();
lw s3, 0x28 (SP)

fuck:
sd r0, 0x807ffff0 //delta time
jal 0x80325070 //osGetTime
nop
DSLL32 v0, v0, 0
daddu v0, v1, v0
sd v0, 0x807ffff8
b noupdate
nop


nofskip:
lb t6, 0x8033b25e
j 0x8024b4b8
nop

paused:
sw v0, 0x1c (SP)

jal 0x80325070 //osGetTime
nop
DSLL32 v0, v0, 0
daddu v0, v1, v0
sd r0, 0x807ffff0 //delta time
sd v0, 0x807ffff8
j 0x8024ba50//-0x80245000
nop


//fixing random crash
crashfix:
sll t9, t8, 2
lui t0, 0x8036
addu t0, t0, t9
lw t0, 0x1320 (T0)
beqz t0, cap
lw t0, 0x80361320

cap:
j 0x802d6de8
lh t1, 0x8 (T0)

.orga 0x802d6dd0-0x80245000
j crashfix+0x7F200000


//update level changes

.orga 0x8024ba00-0x80245000 //play mode paused
jal 0x8024b5d4
nop
j paused+0x7F200000
nop

.orga 0x8024ba14-0x80245000 //play mode change area
jal 0x8024b7c0
nop
jal paused+0x7F200000
nop

.orga 0x8024ba28-0x80245000 //play mode change level
jal 0x8024b880
nop
jal paused+0x7F200000
nop

.orga 0x8024ba3c-0x80245000 //play mode frame advnaced
jal 0x8024b6cc
nop
jal paused+0x7F200000
nop