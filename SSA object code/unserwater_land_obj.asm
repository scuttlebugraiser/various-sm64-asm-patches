//bubble
bubblebhv:

.orga 0x1206400
.dw 0x00040000
.dw 0x11010041
.dw 0x08000000
.dw 0x0c000000
.dw bubbleasm + 0x7F200000
.dw 0x09000000


bubbleasm:
lw t0, 0x80361160
lwc1 f6, 0x15c (t0)
lh at, 0x188 (T0)
mtc1 at, f8
cvt.s.w f8, f8
c.lt.s f8, f6
lui at, 0x8080
lh t4, 0xffe0 (at)
lb t2, 0x18b (t0)
bc1f endB
or t9, t4, t2
nor t8, r0, t2
and t9, t4, t8
endB:
jr ra
sh t9, 0xffe0 (at)