.orga 0x1205e00

.dw 0x00040000
.dw 0x11010041
.dw 0x102a0020
.dw 0x08000000
.dw 0x0c000000
.dw chargeasmstart+0x7F200000
.dw 0x09000000

chargeasmstart:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
lw t0, 0x80361160
lw t1, 0x14c (T0)
lui t3, 0x8034
lbu t4, 0xb198 (T3) //frames since A was pressed
slti t5, t4, 0x80
beql t5, r0, switch
li t4, 0x80
//0 is search mario
//1 is while inside
//2 is after you press A
//3 is when u let go of A
//4 is waiting for you to land

switch:
beq t1, r0, searchmario
li at, 1
beq at, t1, marioinside
li t9, 2
beq t9, t1, primed
li at, 3
beq at, t1, launching
li t9, 4
beq t9, t1, resetscale


wait:
lw t5, 0x154 (T0)
li at, 60
bne at, t5, resetscale
nop
b end
sw r0, 0x14c (T0)

searchmario:
lui at, 0x42d8
lwc1 f0, 0x15c (T0)//dist to mario
mtc1 at, f2
c.lt.s f0, f2
li t8, 1
bc1f end
li a1, 0x130A //talk to NPC
b setaction
sw t8, 0x14c (T0)


marioinside:
lh t6, 0xafa0 (T3)
lh t9, 0xd6 (T0)
andi t6, t6, 0x2000 //Z btn
sh t9, 0xb194 (T3)
bne t6, r0, fallout
sh t9, 0xb19E (T3)
bne t4, r0, nomodel
li t5, 2
b nomodel
sw t5, 0x14c (T0)

primed:
lh t6, 0xafa0 (T3)
andi t7, t6, 0x2000 //Z btn
bne t7, r0, fallout
andi t6, t6, 0x8000 //A btn
bne t6, r0, scale
li t7, 3
sll t4, t4, 1
sh t4, 0x1a4 (T0)
b holdsfx
sw t7, 0x14c (T0)

launching:
li t4, 4
sw t4, 0x14c (T0)

lhu t5, 0x1a4 (T0) //charge time (1 to 255)
mtc1 t5, f6
cvt.s.w f6, f6
li.s f2, 25.0
li.s f0, 1.4
div.s f8, f6, f0
add.s f4, f8, f2 //25.0+hspd
swc1 f8, 0xb1c4 (T3) //h spd
swc1 f4, 0xb1bc (T3) //y spd
lui a1, 0x8033
lui a0, 0x5025 //cannon shot
ori a0, a0, 0xff81
jal 0x8031eb00
ori a1, a1, 0x31f0
lui a1, 0x0100
ori a1, a1, 0x0882 //triple jump
lui t3, 0x8034
b normalmodel
sw a1, 0xb17c (T3)


//if you press Z while inside
fallout:
li a1, 0x0100088C //falling
li at, 5
sw at, 0x14c (T0)
b normalmodel
sw a1, 0xb17c (T3)


//post launch
reset:
lwc1 f0, 0xb1b0 (T3)
swc1 f0, 0xB22C (T3) //remove fall dmg
lw t5, 0x154 (T0)
slti t5, t5, 20
bne t5, r0, end
lw t2, 0xb17c (T3)
andi t2, t2, 0x1c0
li a1, 0x80
beq a1, t2, end
nop
b end
sw r0, 0x14c (T0)


holdsfx:
lw t5, 0x154 (T0)
andi t5, t5, 1
beq t5, r0, end
lui a1, 0x8033
lui a0, 0x305b //elevator
ori a0, a0, 0xFF81
jal 0x8031eb00
ori a1, a1, 0x31f0
b end

scale:
mtc1 t4, f6
cvt.s.w f6, f6
li.s f8, 128.0
li.s f16, 1.0
div.s f2, f6, f8
add.s f2, f2, f16
swc1 f2, 0x2c (T0)
swc1 f2, 0x34 (T0)
li.s f10, 255.0
li.s f12, 2.0
div.s f12, f6, f12
sub.s f14, f10, f12
div.s f10, f14, f10
b holdsfx
swc1 f10, 0x30 (T0)

resetscale:
li.s f2, 1.0
swc1 f2, 0x2c (T0)
swc1 f2, 0x34 (T0)
b reset
swc1 f2, 0x30 (T0)

setaction:
li a0, 0x8033b170
jal 0x80252CF4 //set mario action
li a2, 0x0 //action arg



nomodel:
lw t6, 0x8032ddc4
lw t8, 0x80361158
lw t9, 0x0 (T6)
b end
sw t9, 0x14 (T8)


normalmodel:
lw t6, 0x8032ddc4
lw t8, 0x80361158
lw t9, 0x4 (T6)
sw t9, 0x14 (T8)

end:
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18