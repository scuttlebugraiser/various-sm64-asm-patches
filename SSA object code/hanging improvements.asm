.orga 0x8025ed08-0x80245000
lui at, 0x4140 //12, max hang spd

.orga 0x8025ed1c-0x80245000
lui at, 0x4000 //2, spd gained per frame

.orga 0x8025ef8c-0x80245000
slti at, t2, 8 //time before you can start moving

.orga 0x8025ec4c-0x80245000
b 0x8025ec5c-0x80245000 //don't fall off steep ceilings

.orga 0x8025ec70-0x80245000
b 0x8025ec80-0x80245000 //don't fall off steep ceilings