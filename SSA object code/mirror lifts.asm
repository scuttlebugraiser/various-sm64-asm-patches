.orga 0x12064a0

.dw 0x00040000
.dw 0x11010041
.dw 0x08000000
.dw 0x0c000000
.dw mirrorasm + 0x7F200000
.dw 0x09000000


mirrorasm:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
lw t0, 0x80361160
lw t1, 0x14c (t0)
bne t1, r0, reset
lwc1 f2, 0x15c (t0)
li.s f4, 140.0
c.lt.s f2, f4
lui t8, 0x8034
bc1f end
lwc1 f6, 0xb1bc (t8)
abs.s f6, f6
li.s f8, 1.8
mul.s f6, f6, f8
lwc1 f8, 0xb1c4 (t8)
abs.s f8, f8
li.s f10, 1.2
lw t3, 0x8033b17c
andi t3, t3, 0x1c0
li t4, 0x80
beq t4, t3, lowscale
li.s f12, 1.4
mul.s f10, f12, f10
lowscale:
mul.s f8, f8, f10
add.s f6, f8, f6
sw r0, 0xb1c4 (t8)
swc1 f6, 0xb1bc (t8)
li t9, 1
sw t9, 0x14c (t0)
li a2, 0x01000882
b end
sw a2, 0xb17c (T8)

reset:
lui t5, 0x8034
lwc1 f0, 0xb1b0 (T5)
swc1 f0, 0xB22C (T5) //remove fall dmg
li at, 2
beq t1, at, checkdist
lw t3, 0x8033b17c
andi t3, t3, 0x1c0
li t4, 0x80
beq t4, t3, end
checkdist:
li.s f4, 300.0
c.lt.s f2, f4
li t9, 2
bc1t end
sw t9, 0x14c (T0)

sw r0, 0x14c (t0)

end:
lw t6, 0xd4 (t0)
lw t4, 0xd0 (T0)
addiu t7, t6, 0x40
addiu t3, t4, 0x40
lw t1, 0x14c (T0)
beq t1, r0, norotate
lw t5, 0x154 (t0)
li at, 0x250
subu at, at, t5
blezl at, normal
li at, 0
normal:
addu t3, t3, at
addu t7, t7, at
norotate:
sw t7, 0xd4 (t0)
sw t3, 0xd0 (t0)
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18