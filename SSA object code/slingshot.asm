.orga 0x120a000

.dw 0x00040000
.dw 0x11010041
.dw 0x0c000000
.dw SETPOLEHEIGHT+0x7F200000
.dw 0x2d000000
.dw 0x08000000
.dw 0x0c000000
.dw slingshotasm+0x7F200000
.dw 0x09000000

slingshotasm:

addiu sp, sp, -0x18
sw ra, 0x14 (SP)

lw t0, 0x80361160
lw t1, 0x14c (T0)
//0 for mario not in
//1 for mario inside
//2 for mario is ready to launch
//3 for launching mario
//4 for reset when mario lands
lui T5, 0x8034
li t3, 4
beq t3, t1, @@reset
li t2, 1
beq t1, r0, @@searchmario
lh t3, 0xafa0 (T5)
andi t3, t3, 0x2000 //Z
bne t3, r0, @@fallout
LH t6, 0xC778 (T5) //camera angle
bnel t1, t2, @@lockmario
lh t6, 0x18A (T0)
lh t8, 0xd6 (T0) //obj angle
subu t6, t6, t8
sh t6, 0x18A (T0)

@@lockmario:

//fix lakitu cam state
ori t4, r0, 0x4
sb t4, 0xC685 (T5)

//lock marios angle
lh t7, 0xd6 (T0) //angle
sh t7, 0xb19E (T5)
sh t7, 0xb194 (T5)


ANDI T7, T6, 0xFFF0

SRL T7, T7, 0x2

LUI AT, 0x8038

ADDU A0, AT, T7
L.S F0, 0x7000 (A0)//cos_u16

L.S F2, 0x6000 (A0)//sin_u16


lh t7, 0xaf90 (T5)//horiz tilt
mtc1 t7, f10

lh t4, 0xaf92 (T5)//vert tilt
mtc1 t4, f8
cvt.s.w f8, f8 //H tilt
cvt.s.w f10, f10 //V tilt

mul.s f6, f8, f0
mtc1 r0, f24
mul.s f4, f10, f2
add.s f22, f4, f6
c.lt.s f22, f24
mul.s f18, f6, f6
bc1f @@noskipforwardstick
li at, 1
bnel t1, at, @@capstickmag
mtc1 r0, f12
b @@setanim

@@noskipforwardstick:
mul.s f20, f4, f4
add.s f12, f20, f18

sqrt.s f12, f12 //pull back

@@capstickmag:
mul.s f6, f10, f0 //V tilt component
mul.s f4, f8, f2 //H tilt component
mul.s f18, f6, f6
mul.s f20, f4, f4
add.s f14, f20, f18
sqrt.s f14, f14 //transverse tilt

//lock mario in slingshot

lwc1 f16, 0x00a4 (T0) //obj y pos
li.s f18, 50.0
add.s f16, f16, f18
swc1 f16, 0xb1b0 (T5) //mario y pos

//tilt angle
lh t7, 0xd6 (T0) //angle
li.s f8, 40.0
add.s f4, f6, f4
mtc1 r0, f30
c.lt.s f4, f30
lh a0, 0xd6 (T0) //obj angle
bc1tl @@nonegatetilt
neg.s f14, f14

@@nonegatetilt:
mul.s f14, f8, f14
cvt.w.s f14, f14
mfc1 t6, f14
addu t7, t7, t6
sh t7, 0xb19E (T5)
sh t7, 0xb194 (T5)




ANDI a0, a0, 0xFFF0
SRL a0, a0, 0x2
LUI AT, 0x8038
ADDU A0, AT, a0
L.S F26, 0x7000 (A0)//cos_u16
L.S F28, 0x6000 (A0)//sin_u16

//pull back deflection
//50 up
//105 back
neg.s f10, f12

li.s f0, -110
mul.s f2, f28, f0
li.s f24, 2.0

lwc1 f16, 0x0164 (T0) //obj x home
mul.s f18, f10, f28
mul.s f18, f18, f24
add.s f18, f18, f16
add.s f20, f18, f2
swc1 f20, 0xb1ac (T5) //mario x pos
swc1 f18, 0x00a0 (T0) //obj z pos

mul.s f2, f26, f0
lwc1 f16, 0x016c (T0) //obj z home
mul.s f18, f10, f26
mul.s f18, f18, f24
add.s f18, f18, f16
add.s f20, f2, f18
swc1 f20, 0xb1b4 (T5) //mario z pos
swc1 f18, 0x00a8 (T0) //obj z pos

cvt.w.s f12, f12
mfc1 t4, f12


li t2, 1
beq t2, t1, @@marioinside
li t3, 2
beq t3, t1, @@primed
li t2, 3
beq t2, t1, @@launching
lui t3, 0x8034

@@searchmario:
lui at, 0x42d8
lwc1 f0, 0x15c (T0)//dist to mario
mtc1 at, f2
c.lt.s f0, f2
li t8, 1
bc1f @@end
li t9, 0x130A //reading sign
sw t8, 0x14c (T0)
lui t7, 0x8034
b @@setanim
sw t9, 0xb17c (T7)

@@marioinside:
lw t5, 0x154 (T0)
subiu t6, t5, 0x4
blez t6, @@holdsfx
subiu t3, t4, 0x40
blez t3, @@holdsfx
li t5, 2
b @@holdsfx
sw t5, 0x14c (T0)

@@primed:
subiu t3, t4, 0x34
bgez t3, @@holdsfx
li t5, 3
b @@holdsfx
sw t5, 0x14c (T0)


@@launching:
lw t5, 0x154 (T0)
beq t5, r0, @@holdsfx
li t6, 0x8
li t7, 1
beql t5, t6, @@holdsfx
sw t7, 0x14c (T0)

subiu t2, t4, 0x10
bgez t2, @@setanim
li t4, 4
sw t4, 0x14c (T0)
mtc1 t5, f6
cvt.s.w f6, f6
lui at, 0x4248 //40
mtc1 at, f0
lui at, 0x4244
mtc1 at, f2
div.s f8, f0, f6 //40/time to flick
add.s f4, f0, f8 //40 + 40/time
swc1 f4, 0xb1c4 (T3) //h spd
swc1 f2, 0xb1bc (T3) //y spd
li t2, 0x01000882 //special triple jump
sw t2, 0xb17c (T3)
lui a1, 0x8033
lui a0, 0x306C //boing
ori a0, a0, 0x0081
jal 0x8031eb00
ori a1, a1, 0x31f0
b @@setanim


@@fallout:
li at, 0x0100088C
sw at, 0xb17c (T5)
li at, 4
sw at, 0x14c (T0)
lwc1 f16, 0x0164 (T0) //obj x home
swc1 f16, 0x00a0 (T0) //obj x pos
lwc1 f16, 0x016C (T0) //obj z home
swc1 f16, 0x00a8 (T0) //obj z pos

@@reset:
li t7, 0x80 //in air
lwc1 f0, 0xb1b0 (T5)
lw t4, 0xb17c (T5)
andi t6, t4, 0x1c0
beq t7, t6, @@end
swc1 f0, 0xB22C (T5) //remove fall dmg
b @@end
sw r0, 0x14c (T0)

@@holdsfx:
lw t5, 0x154 (T0)
slti t6, t4, 0x15
bne t6, r0, @@setanim
slti t7, t4, 0x30
and t5, t5, t7
bne t5, r0, @@setanim
lui a1, 0x8033
lui a0, 0x0008 //normal
ori a0, a0, 0xFF81
jal 0x8031eb00
ori a1, a1, 0x31f0


@@setanim:
li a0, 0x8033b170
jal 0x802509b8 //set mario anim
li a1, 0xc0 //anim ID

@@end:
jal stretchband+0x7F200000
nop
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18

.orga 0x120A350
POLEBEHAV:
.DW 0x00080000
.DW 0X11010001
.DW 0X0A000000

SETPOLEHEIGHT:

addiu sp, sp, -0x18
sw ra, 0x14 (SP)


lw a0, 0x80361160
li a2, POLEBEHAV+0x7F200000-0x80000000
jal 0x8029edcc
li a1, 0xfc

lw a0, 0x80361160
li a2, POLEBEHAV+0x7F200000-0x80000000
jal 0x8029edcc
li a1, 0xfd

lw a0, 0x80361160
sw A0, 0x68 (V0)
sw v0, 0x1e4 (A0)
li a2, POLEBEHAV+0x7F200000-0x80000000
jal 0x8029edcc
li a1, 0xff

lw t0, 0x80361160
jal 0x802a064c //find floor height, store in 0xE8
sw v0, 0x1e0 (T0)
//T7 is this obj from prev function
lwc1 f0, 0xe8 (T7)
//height of obj is 140
lwc1 f2, 0xa4 (T7)
sub.s f4, f2, f0 //distance to ground
li.s f6, 140
div.s f4, f4, f6
lw v0, 0x1e0 (T7)
swc1 f4, 0x30 (V0) //y scale


lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18




stretchband:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
lw t0, 0x80361160
lw t1, 0x1e4 (T0) //bands
lwc1 f0, 0x164 (T0) //x home
lwc1 f2, 0x16C (T0) //z home
lwc1 f4, 0xa0 (T0)
lwc1 f6, 0xa8 (T0)
sub.s f0, f4, f0 //x change
sub.s f2, f6, f2 //z change
mul.s f10, f0, f0
mul.s f12, f2, f2
add.s f12, f12, f10
sqrt.s f12, f12 //magnitude of change
li.s f10, 50
div.s f12, f12, f10
li.s f14, 1.0
add.s f12, f12, f14
swc1 f12, 0x34 (T1)
lwc1 f16, 0x16c (T0)
sub.s f16, f16, f2
swc1 f16, 0xa8 (T1)
lwc1 f16, 0x164 (T0)
sub.s f16, f16, f0
swc1 f16, 0xa0 (T1)
; A0 = cmdF2 seg address
; A1 = spd x
; A2 = spd y
//pillar
lw AT, 0x8032D580 //current VI
beq at, s6, noscrollF2
li a0, 0x0403d610
or s6, r0, at
li a1, 1
jal 0x8040f100 //aglab f2 scroll
li a2, 0

//bands
li a0, 0x04040ba0
li a1, 1
jal 0x8040f100 //aglab f2 scroll
li a2, 0

noscrollF2:
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18
