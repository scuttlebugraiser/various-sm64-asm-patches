.orga 0x1203dd8

.dw 0x00060000
.dw 0x11010001
.dd 0x2f00000000000010
.dw 0x21000000
.dw 0x2b000000
.dw 0x00500050 //radius/height
.dw 0x00000000 //vert offset
.dw 0x08000000
.dw 0x0f1A0001
.dd 0x0c00000080403E10
.dw 0x10050000
.dw 0x09000000

.orga 0x1203E10
ADDIU SP, SP, -0X18
SW RA, 0X14 (SP)
lw t0, 0x80361160
lw t2, 0x134 (T0)
beq t2, r0, end
lw t6, 0x6c (T0)
lw t7, 0x1b4 (T0)
beq t6, t7, star
LHU T5, 0X18A (T0) //TIME
LW T6, 0X1B0 (T0)
BEQ T5, T6, RESET
ADDIU T6, T6, 0X1
SW T6, 0X1B0 (T0)
lbu t8, 0x189 (T0)
beq t8, r0, nomusic
lw t9, 0x14c (T0)
bne t9, r0, nomusic
or a1, t8, r0
jal 0x80320544 //play music
or a0, r0, r0
LW T0, 0X80361160
nomusic:
LI T9, 0X23
SW T9, 0X14C (T0)
SH T9, 0X2 (T0) //MAKE INVIS
lw t5, 0x154 (T0)
li t6, 0x10
divu t5, t6
mfhi t5
bne t5, r0, end
lbu t8, 0x189 (T0)
bne t8, r0, end
LUI A0, 0X3054 //switch tick sound
JAL 0X802CA144
ORI A0, A0, 0X81
BEQ R0, R0, END
NOP

RESET:
sw r0, 0x1b4 (T0)
SW R0, 0X134 (T0)
SW R0, 0X1B0 (T0)
SW R0, 0X14C (T0)
li t9, 0x25
beq r0, r0, end
SH t9, 0X002 (T0)

star:
sh r0, 0x74 (t0)
lwc1 f12, 0Xa0 (T0) //this x
lwc1 f14, 0Xa4 (T0) //this y
jal 0X802F2B88 //spawn a star
lw a2, 0Xa8 (T0) //this z
lw t0, 0x80361160
lbu t9, 0x188 (T0)
sw t9, 0x188 (V0)



end:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18


.ORGA 0X1203EF4

.dw 0x00060000
.dw 0x11010001
.dd 0x2f00000000000010
.dw 0x21000000
.dw 0x2b000000
.dw 0x00500050 //radius/height
.dw 0x00000000 //vert offset
.dd 0x0c00000080403F34
.dw 0x08000000
.dw 0x0f1A0001
.dw 0x10050000
.dd 0X0C00000080403F70
.dw 0x09000000


.ORGA 0X1203F34

ADDIU SP, SP, -0X18
SW RA, 0X14 (SP)

lui a0, 0X0040
jal 0X8029F95C //find obj
addiu a0, a0, 0X3DD8
beq r0, v0, noparent
lui t0, 0X8036
lw t0, 0X1160 (T0)
sw v0, 0X6c (T0)
lw t1, 0x6c (V0)
addiu t1, t1, 0x1
sw t1, 0x6c (V0)

noparent:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18

//every frame

.ORGA 0X1203F70
ADDIU SP, SP, -0X18
SW RA, 0X14 (SP)

LW T0, 0X80361160
LW v0, 0X6C (T0) //PARENT
LW T1, 0X14C (V0)
BEQ T1, R0, RESETC
LW T3, 0X14C (T0)
BNE T3, R0, INVIS
LW T2, 0X134 (T0)
BNE T2, R0, COLLECT
li t9, 0x25
SH t9, 0X2 (T0)
BEQ R0, R0, COIN
SW R0, 0X9C (T0) //ENABLE COL

RESETC:
beq r0, r0, invis
sw r0, 0x14c (t0)


COLLECT:
LW T9, 0X1b4 (V0) //NUM OF REDS
ADDIU T9, T9, 0X1
SW T9, 0X1b4 (V0)
SW T9, 0X14C (T0)
LUI A0, 0X3011 //coin sound
JAL 0X802CA144
ORI A0, A0, 0X81

INVIS:
lw t0, 0x80361160
sw r0, 0x134 (T0)
LI T9, 0X23
SH T9, 0X2 (T0) //INVIS
SW T9, 0X9C (T0) //NO COL


COIN:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18