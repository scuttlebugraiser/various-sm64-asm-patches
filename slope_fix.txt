.orga 0xfe2f8
lui t0, 0xdfff
sw t0, 0x20 (SP)
looppoint:
lw t0, 0x4 (A0)
lw a0, 0x0 (A0)
lh t1, 0xA (T0)
lh t2, 0xE (T0)
lh t3, 0x10 (T0)
lh t4, 0x14 (T0)
subu t6, t2, a3
subu t7, t3, t1
multu t6, t7
mflo t8
subu t6, t4, t2
subu t9, t1, a1
multu t9, t6
mflo t7
subu t9, t8, t7
bgez t9, yes
nop
beq r0, r0, loop
nop
yes:

.orga 0xfe49c
div.s f22,f4,f10
mtc1 a2, f4
lui at, 0xc29c
mtc1 at, f18
cvt.s.w f10,f4
lwc1 f24, 0x20 (SP)
c.lt.s f22,f24
add.s f16,f22,f18
mtc1 r0, f8
sub.s f6, f10, f16
bc1T loop
nop
c.lt.s f6, f8
nop
bc1f hit
nop
b loop
nop
hit:
lw t8, 0x48 (SP)
sw t0, 0x4 (SP)
swc1 f22, 0x20 (SP)
swc1 f22, 0x0 (T8)
loop:
bnez a0, looppoint