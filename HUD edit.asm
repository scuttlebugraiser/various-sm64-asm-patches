;coin x pos
.orga 0x802e37b8-0x80245000
addiu a0, r0, 0x68

;cross x pos
.orga 0x802e37cc-0x80245000
addiu a0, r0, 0x78

;number x pos
.orga 0x802e37e8-0x80245000
addiu a0, r0, 0x86


;star x pos
.orga 0x802e386c-0x80245000
addiu a0, r0, 0xc2

;cross x pos
.orga 0x802e3890-0x80245000
addiu a0, r0, 0xd2

;number x pos
.orga 0x802e38b8-0x80245000
addiu a0, a0, 0xd4


;cam pos
.orga 0x802e3bb8-0x80245000
addiu a0, r0, 0xc2
addiu a1, r0, 0xa4

;mario icon pos
.orga 0x802e3c0c-0x80245000
addiu a0, r0, 0xd2
addiu a1, r0, 0xa4

;x icon pos
.orga 0x802e3c2c-0x80245000
addiu a0, r0, 0xd2
addiu a1, r0, 0xa4

;down arrow pos
.orga 0x802e3ca0-0x80245000
addiu a0, r0, 0xd6
addiu a1, r0, 0xb2

;up arrow pos
.orga 0x802e3cc4-0x80245000
addiu a0, r0, 0xd6
addiu a1, r0, 0x96