//hit floor:
.orga 0x802f5e74-0x80245000
jal createdeployment+0x7F200000 //my own spawn function
li a0, 0
b 0x802f5f38-0x80245000

//hit slope
.orga 0x802f5e90-0x80245000
jal createdeployment+0x7F200000 //my own spawn function
li a0, 0
b 0x802f5f38-0x80245000

//hit wall:
.orga 0x802f5ed8-0x80245000

jal createdeployment+0x7F200000 //my own spawn function
li a0, 1
b 0x802f5f38-0x80245000


//flash time:
.orga 0x802f5f70-0x80245000

slti at, t0, 100

//disappear time:
.orga 0x802f5fd0-0x80245000

slti at, t0, 200


//respawn time:
.orga 0x802f5fec-0x80245000


ori a2, r0, 100

.orga 0x802f6090-0x80245000

ori a2, r0, 100

//y spd on throw
.orga 0x802f61c4-0x80245000

li.s f6, 34.0
nop//lwc1 f4, 0x8033b1bc
nop//add.s f6, f4, f6
swc1 f6, 0xb0 (T3)




//gravity 0xe4
//friction 0x170
//bouyancy 0x174
.orga 0x802f5cdc-0x80245000

lw t0, 0x80361160

li.s f4, 1.4//gravity
swc1 f4, 0xe4 (T0)

li.s f4, 0.99//friction
swc1 f4, 0x170 (T0)

li.s f4, 0.5//bouyancy
swc1 f4, 0x174 (T0)
b 0x802f5d1c-0x80245000
nop


//my own spawn function aka deploymed platform
.orga 0x1208500
createdeployment:
ADDIU SP, SP, -0X28
SW RA, 0X28 (SP)
sw a0, 0x24 (SP)
lw a0, 0x80361160
lw t1, 0xf4 (A0)
li at, 1
bne t1, at, nodeployment
li a2, 0x00408700
JAL 0X8029EDCC
li a1, 0xd9
lw a0, 0x80361160

lwc1 f2, 0xb8 (A0)
swc1 f2, 0xb8 (V0)


lwc1 f2, 0x164 (A0)
lwc1 f4, 0x168 (A0)
lwc1 f6, 0x16C (A0)

swc1 f2, 0x164 (V0)
swc1 f4, 0x168 (V0)
swc1 f6, 0x16C (V0)
sh r0, 0x74 (A0)
li.s f8, 2.5
swc1 f8, 0x30 (V0) //y scale

li.s f8, 0.6
swc1 f8, 0x34 (V0) //z scale

li.s f8, 100.0
swc1 f8, 0x128 (V0) //wallhitboxradius

lw a0, 0x24 (SP)
sw a0, 0x188 (V0)



//a0 *parent
//a1 useless
//a2 model
//a3 *bhv
//s16 x,y,z,rx,ry,rz (in order starting at 0x10 in SP, one word each)

cvt.w.s f2, f2
mfc1 t2, f2
sw t2, 0x10 (SP)
cvt.w.s f4, f4
mfc1 t4, f4
sw t4, 0x14 (SP)
cvt.w.s f6, f6
mfc1 t6, f6
sw t6, 0x18 (SP)
sw r0, 0x1c (SP)
sw r0, 0x20 (SP)
sw r0, 0x24 (SP)
or a0, r0, v0
li a3, 0x00408880
JAL 0X8029e9ac //spawn_object_abs_with_rot
li a2, 0x8c
lw t6, 0x68 (V0)
sw V0, 0x68 (T6)

lui a1, 0x8033
lui a0, 0x3056 //rock
ori a0, a0, 0xFF81
jal 0x8031eb00
ori a1, a1, 0x31f0



nodeployment:
LW RA, 0X28 (SP)
JR RA
ADDIU SP, SP, 0X28



//respawn breakable box small
createbreakable:
ADDIU SP, SP, -0X18
SW RA, 0X14 (SP)

lw a0, 0x80361160
lwc1 f2, 0x164 (A0)
lwc1 f4, 0x168 (A0)
lwc1 f6, 0x16C (A0)

swc1 f2, 0xa0 (A0)
swc1 f4, 0xa4 (A0)
swc1 f6, 0xa8 (A0)

li a2, 0x13004218
JAL 0X8029EDCC
li a1, 0x82
lw a0, 0x80361160
lw t6, 0x68 (A0)
sh r0, 0x74 (T6)
sh r0, 0x74 (A0)


LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18


.orga 0x1208700
deploymentbhv:

.dw 0x00090000
.dw 0x11010001
.dw 0x2a000000
.dw 0x08024c28
.dw 0x08000000
.dw 0x0c000000
.dw deploymentasm+0x7F200000
.dw 0x09000000



deploymentgfx:
ADDIU SP, SP, -0X18
SW RA, 0X14 (SP)
//a0 is 0 for floor
//a0 is 1 for wall
lw a0, 0x80361160
lw a0, 0x188 (A0)
bnez a0, wall
nop
jal 0x802a4f58// obj_align_gfx_with_floor
nop
b spawn
nop
wall:
jal 0x802a1f3c //obj_resolve_wall_collisions
nop //0x1b4 is wall angle
lw t0, 0x80361160
lw a0, 0x1b4 (T0)
sw a0, 0xd4 (T0)

li at, 0x4000
sw at, 0xd0 (T0)

spawn:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18


deploymentasm:
ADDIU SP, SP, -0X18
SW RA, 0X14 (SP)
jal deploymentgfx+0x7F200000
nop
jal 0x802A3754 //is mario gp on this obj
nop
bne v0, r0, deletedeploy
nop
b enddeploy
nop

deletedeploy:
jal createbreakable+0x7F200000
nop
b enddeploy

enddeploy:
lw t0, 0x80361160
lui t9, 0x8034
li at, 0x00020339
lw t3, 0xb17c (T9)
beq at, t3, nosolid
lw t1, 0xb170+0x64 (T9)
beq t1, r0, solid
li at, 0x8080
lw t1, 0x2c (T1)
lui t0, 0x8034
beql t1, t0, nosolid
sh at, 0xb170+0xae (T9)
solid:
jal 0x803839cc
nop
b returndeploy
nosolid:
li.s f12, 250.0
jal 0x802a3818 //obj_push_mario_away
nop
returndeploy:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18



.orga 0x1208880
returnbhv:

.dw 0x00090000
.dw 0x11010001
.dw 0x2a000000
.dw 0x08000e98
.dw 0x32000160
.dw 0x2d000000
.dw 0x08000000
.dw 0x0c000000
.dw deploymentasm+0x7F200000
.dw 0x09000000


//y spd = 34
//h spd = 40
//gravity = 1.4
//radius = 150
//make one step 2.5 frames worth
//spawn a dashed line every 8 steps
//dashed line just exists for one frame I guess
//instead of actually spawning several new objects, just have a set of
//objects that you set to a position


.orga 0x1208900
markerbhv:
//use model ID 0x83
.dw 0x00040000
.dw 0x11010001
.dw 0x101a0001
.dw 0x0e457fff
.dw 0x32000260
.dw 0x0e280064 //wall hitbox
.dw 0x08000000
.dw 0x0c000000
.dw markerasm+0x7F200000
.dw 0x09000000

markerasm:
ADDIU SP, SP, -0X38
SW RA, 0X14 (SP)
lui a0, 0x1300
jal 0x80277f50
ori a0, a0, 0x4218
lw t0, 0x8033b170+0x7c
beq t0, r0, invis
nop
lw t1, 0x20c (T0) //bhv ptr
bne v0, t1, invis
//only calc every other frame
lw t0, 0x80361160
lw t5, 0x154 (T0)
andi t5, t5, 1
beq t5, r0, hitcheck
lw t3, 0x1c8 (T0)

//calculate trajectory now
li.s f0, 50.0
lui at, 0x8034
lwc1 f2, 0xb1ac (AT)
lwc1 f4, 0xb1b0 (AT)
lwc1 f6, 0xb1b4 (AT)
add.s f4, f4, f0
lh a0, 0xb19e (AT) //rotation
//addiu a0, a0, 0xc000
//sin
ANDI A1, A0, 0xFFF0
SRL A1, A1, 0x2
LUI AT, 0x8038
ADDU A1, AT, A1
L.S F12, 0x6000 (A1)

//cos
ANDI A0, A0, 0xFFF0
SRL A0, A0, 0x2
LUI AT, 0x8038
ADDU A0, AT, A0
L.S F10, 0x7000 (A0)

li.s f14, 120.0
mul.s f12, f12, f14 //z term of spd
li.s f16, 34.0
mul.s f10, f10, f14 //x term of spd
lwc1 f18, 0x8033b1bc
add.s f16, f18, f16

li.s f18, 1.4
li.s f20, 3.0
li.s f14, 0.0
lw t0, 0x80361160
li at, 0x21
sh at, 0x2 (T0)
li.s f0, 1.0
li.s f8, 2.0
//make steps
//Y=nV-a*n(n-1)/2+Y0
//V=34.0+mario y spd=f16
//a=1.4=f18
//n=f14
//Y0=f4
//f2 is x pos
//f6 is z pos
//1=f0
//2=f8
//temp n=f26
//temp a=f22
//call obj_update_floor_height_and_get_floor each step, then compare floor height
//call obj_resolve_wall_collisions each step and just check if it returns 1

posloop:
add.s f2, f2, f12
add.s f6, f6, f10
swc1 f2, 0xa0 (T0)
swc1 f6, 0xa8 (T0)
add.s f14, f14, f20
mul.s f24, f16, f14//n*V
mul.s f22, f18, f14//a*n
sub.s f26, f14, f0//n-1
mul.s f22, f22, f26//a*n(n-1)
div.s f22, f22, f8//a*n(n-1)/2
sub.s f24, f24, f22//n*V-a*n(n-1)/2
add.s f24, f24, f4//n*V-a*n(n-1)/2+Y0
swc1 f24, 0xa4 (T0)

//store shit in stack
swc1 f16, 0x18 (SP)
swc1 f18, 0x1c (SP)
swc1 f10, 0x20 (SP)
swc1 f12, 0x24 (SP)
swc1 f4, 0x28 (SP)
swc1 f14, 0x2c (SP)
swc1 f20, 0x30 (SP)
swc1 f0, 0x34 (SP)
swc1 f8, 0x38 (SP)



//obj positions are set, now check for collision

jal 0x802a064c //obj_update_floor_height_and_get_floor
nop
beq v0, r0, wallhit
lw t0, 0x80361160
lwc1 f22, 0xe8 (T0) //floor height
lwc1 f14, 0xa4 (T0)
c.lt.s f22, f14
nop
bc1f floorhit
li at, 1
jal 0x802a1f3c //wall detection (0 =false, 1 = true)
nop
lw t0, 0x80361160
bne v0, r0, wallhit
nop
lwc1 f16, 0x18 (SP)
lwc1 f18, 0x1c (SP)
lwc1 f10, 0x20 (SP)
lwc1 f12, 0x24 (SP)
lwc1 f4, 0x28 (SP)
lwc1 f14, 0x2c (SP)
lwc1 f20, 0x30 (SP)
lwc1 f0, 0x34 (SP)
lwc1 f8, 0x38 (SP)
lwc1 f2, 0xa0 (T0)
b posloop
lwc1 f6, 0xa8 (T0)

hitcheck:
lw t0, 0x80361160
lw t1, 0x1c8 (T0)
beq t1, r0, wallhit
li at, 1

//hit is found
floorhit:
lw t0, 0x80361160
jal 0x802a4f58// obj_align_gfx_with_floor
sw at, 0x1c8 (T0)
b endindicator
nop
wallhit:
lw t0, 0x80361160
jal 0x802a1f3c //obj_resolve_wall_collisions
sw r0, 0x1c8 (T0)
lw t0, 0x80361160
lw a0, 0x1b4 (T0)
sw a0, 0xd4 (T0)
li at, 0x4000
b endindicator
sw at, 0xd0 (T0)

invis:
lw t0, 0x80361160
li at, 0x23 //invis
sh at, 0x2 (T0)

endindicator:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X38