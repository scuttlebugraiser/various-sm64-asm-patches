//80380300, 0x36 level script cmd, set level music

//no run yet
.orga 0x000FD080//80380300
lui t0, 0x8039
lh t6, 0xb8ac (T0)//current area
li at, -1
beq t6, at, skipcmd
lw t7, 0xbe28  (T0)//*current level script cmd
lw t9, 0x8032ddc8 //gAreas
sll t1, t6, 4
lh t8, 0x2 (T7) //music param (no idea)
subu t1, t1, t6
sll t1, t1, 2
addu t2, t9, t1 //*current area struct
sh t8, 0x36 (T2)
lb t1, 0x80331620 //act number
addu t6, t7, t1
lb t8, 0x3 (T6) //music param 2 (music ID I guess)
sh t8, 0x38 (T2)

skipcmd:
lbu t1, 0x1 (T7) //level cmd length
addu t1, t1, t7
sw t1, 0xbe28 (T0)
jr ra
nop