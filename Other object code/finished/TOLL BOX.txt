.orga 0x1203FF4

.dw 0x00090000
.dw 0x11010001
.dd 0x2a00000008024c28
.dw 0x08000000
.dd 0x0c000000803839cc
.dd 0x0c00000080404040
.dw 0x09000000



.orga 0x1204040

ADDIU SP, SP, -0X18
SW RA, 0X14 (SP)

lw t0, 0x80361160
lw t4, 0x188 (T0)
beq t4, r0, delete


LUI A0, 0X8036
LW A1, 0X1158 (A0)
JAL 0X8029E2F8 //DISTANCE 3D, RETURN F0
LW A0, 0X1160 (A0)
CVT.W.S F0, F0
MFC1 T8, F0
SUBIU T8, T8, 0X200
BGEZ T8, END

//print
lw t0, 0x80361160
lw t4, 0x188 (T0)
ori t2, r0, 0x64 //100
div t4, t2
mflo t7 //quotient = 100's place
mfhi t4 //tens + ones place
ori t1, r0, 0X000a //10
div t4, t1
mflo t6 //quotient = tens place
mfhi t5 //remainder = ones place
addiu t5, t5, 0X0030
addiu t6, t6, 0X0030
addiu t7, t7, 0x0030
lui a2, 0X8040
sb t7, 0x4024 (A2)
sb t6, 0X4025 (A2)
sb t5, 0X4026 (A2)
ori a2, a2, 0X401c
li a1, 0X00C0
jal 0X802D62d8 //print string
li a0, 0X0060


lui a2, 0X8040
ori a2, a2, 0X4029
li a1, 0X00a0
jal 0X802D62d8 //print string
li a0, 0X0055


lw t0, 0x80361160
lw t5, 0x188 (T0) //coins needed
lui t9, 0x8034
lh t7, 0xb218 (T9) //coins
subu t6, t7, t5
bltz t6, end
lh t8, 0xafa0 (T9)
andi t8, t8, 0x20
beq t8, r0, end
nop
sh t6, 0xb218 (T9)
sh t6, 0xb262 (T9) //incremental coins
delete:
sh r0, 0x74 (T0)
LUI A0, 0X506d //eyerock explosion
JAL 0X802CA144
ORI A0, A0, 0X81
lui a2, 0X3f33
ori a2, a2, 0X3333
addiu a0, r0, 0X0010 //number of objects
jal 0X802ae0cc //explosion code
addiu a1, r0, 0X008b //model ID, 8B=star


END:
LW RA, 0X14 (SP)
JR RA
ADDIU SP, SP, 0X18