//802473c8 display_frame_buffer
//8037b24c init_graph_node_root
//8024784c draw_screen_borders
//80246e70 my_rdp_init
//802474b8 clear_frame_buffer
//80246cf0 thread1_idle
//80247db4 func_80247D84
//80247284  clear_z_buffer
//8037b2b4 - x defined
//0x372bc0 - y defined
//8037b2cc - half width defined
//8037b2d8 - half height defined

width equ 256
height equ 192
//graph node stuff

.orga 0x8037b2ac-0x80283280 //graph node root
li t1, width/2

.orga 0x8037b2b8-0x80283280 //graph node root
li t3, height/2

.orga 0x8037b2c4-0x80283280 //graph node root
li t5, width/2

.orga 0x8037b2d0-0x80283280 //graph node root
li t7, height/2


//display stuff
.orga 0x80246efc-0x80245000 //my rdp init
lui t1, (width<<2)/16
ori t1, t1, height<<2

.orga 0x80247338-0x80245000 //clear z buffer color image
ori t4, t4, width-1

.orga 0x80247448-0x80245000 //display frame buffer set c img change
ori t8, t8, width-1

.orga 0x8024749c-0x80245000 //display FB set scissor
lui t8, (width<<2)/16
ori t8, t8, (height<<2)-32

;This may cause some noise at the top&bottom of screen
.orga 0x8024784c-0x80245000 //black bars
jr ra//addiu sp, sp, -0x20
nop//lui t6, 0x8034

//VI stuff

.orga 0x000F0010+0xa0+8 //vi mode NTSC LAN1
.dw width //width

.orga 0x000F0010+0xa0+0x20 //vi mode NTSC LAN1
.dw (width*512)/320 //xscale
nop
.dw width*2 //origin[0]
.dw (height*1024)/240 //yscale[0]

.orga 0x000F0010+0xa0+0x3c //vi mode NTSC LAN1
.dw width*2 //origin[1]
.dw (height*1024)/240 //yscale[1]

//.orga 0x000F0010+0x500 //vi mode PAL LAN1
//.dw width